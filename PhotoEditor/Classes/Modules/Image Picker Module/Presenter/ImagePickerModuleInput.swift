import Foundation

protocol ImagePickerModuleInput: class {
  
  func configure(with source: ImagePickerSource)
  
}
