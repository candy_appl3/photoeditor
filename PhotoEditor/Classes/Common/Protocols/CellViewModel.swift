import UIKit

protocol CellViewAnyModel {
  static func cellClass() -> UIView.Type
  func setupDefault(on cell: UIView)
  func updateAppearance(of view: UIView, in parentView: UIView, at indexPath: IndexPath)
}

extension CellViewAnyModel {
  func updateAppearance(of view: UIView, in parentView: UIView, at indexPath: IndexPath) { }
}

protocol CellViewModel: CellViewAnyModel {
  associatedtype CellClass: UIView
  associatedtype CellObject: PlainObject
  var cellObject: CellObject { get }
  init(cellObject: CellObject)
  func setup(on cell: CellClass)
}

extension CellViewModel {
  
  static func cellClass() -> UIView.Type {
    return Self.CellClass.self
  }
  
  func setupDefault(on cell: UIView) {
    setup(on: cell as! Self.CellClass) 
  }
}

protocol PlainObject { }
