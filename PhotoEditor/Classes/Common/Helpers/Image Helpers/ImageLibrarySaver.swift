import UIKit

class ImageLibrarySaver: ImageSaver {
  
  private var handler: ((Bool) -> ())!
  
  func save(image: UIImage, completion: @escaping (Bool) -> ()) {
    handler = { completion($0) }
    UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
  }
  
}
