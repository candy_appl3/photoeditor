import UIKit

struct HorizontalFlipStrategy: EditorStrategy {
  
  func edit(image: UIImage) -> UIImage {
    guard let cgImage = image.cgImage else { return image }
    return UIImage(cgImage: cgImage, scale: 1.0, orientation: .upMirrored)
  }
  
}
