import Foundation

protocol ImagePickerViewOutput {
  func viewIsReady()
  func imagePickerDidCancel()
  func didFinishPickingMedia(with info: [String: Any])
}
