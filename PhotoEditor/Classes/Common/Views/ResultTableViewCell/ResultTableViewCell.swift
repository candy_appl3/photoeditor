import UIKit

class ResultTableViewCell: UITableViewCell {

  let resultImageView: UIImageView! = {
    let image = UIImageView()
    image.clipsToBounds = true
    image.contentMode = .scaleAspectFill
    image.translatesAutoresizingMaskIntoConstraints = false
    image.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    return image
  }()
  
  let filterLabel: UILabel! = {
    let label = UILabel()
    label.minimumScaleFactor = 0.5
    label.lineBreakMode = .byTruncatingTail
    label.textAlignment = .center
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  private func setup() {
    setupResultImageView()
    setupFilterLabel()
  }
  
  private func setupResultImageView() {
    contentView.addSubview(resultImageView)
    NSLayoutConstraint.activate([
      resultImageView.topAnchor
        .constraint(equalTo: contentView.topAnchor, constant: Constants.offset),
      resultImageView.bottomAnchor
        .constraint(equalTo: contentView.bottomAnchor, constant: -Constants.offset),
      resultImageView.leftAnchor
        .constraint(equalTo: contentView.leftAnchor, constant: Constants.offset),
      resultImageView.widthAnchor.constraint(equalTo: resultImageView.heightAnchor, multiplier: 1)
    ])
  }
  
  private func setupFilterLabel() {
    contentView.addSubview(filterLabel)
    NSLayoutConstraint.activate([
      filterLabel.leftAnchor
        .constraint(equalTo: resultImageView.rightAnchor, constant: Constants.offset),
      filterLabel.rightAnchor
        .constraint(equalTo: contentView.rightAnchor, constant: Constants.offset),
      filterLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
    ])
  }
}
