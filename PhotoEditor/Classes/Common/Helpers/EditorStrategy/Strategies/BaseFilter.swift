import CoreImage
import UIKit

struct BaseFilter {
  
  static func edit(with name: String, image: UIImage) -> UIImage? {
    let context = CIContext(options: nil)
    
    if let currentFilter = CIFilter(name: name) {
      let beginImage = CIImage(image: image)
      currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
      if let output = currentFilter.outputImage,
        let cgimg = context.createCGImage(output, from: output.extent) {
        return UIImage(cgImage: cgimg)
      }
    }
    return nil
  }
  
}
