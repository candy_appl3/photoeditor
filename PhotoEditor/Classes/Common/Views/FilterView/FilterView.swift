import UIKit

class FilterView: UIView {
  
  //MARK: - Properties
  private var displayColletion: TableViewDisplayCollection<FilterTableViewCellModel>!
  weak var delegate: FilterViewDelegate?
  
  var image: UIImage? {
    didSet {
      photoImage.image = image
    }
  }
  
  var pickTitle: String? {
    didSet {
      pickButton.setTitle(pickTitle, for: .normal)
    }
  }
  
  var filters: [FilterPlainObject]! {
    didSet {
      displayColletion.items = filters
    }
  }
  
  private let photoImage: UIImageView! = {
    let image = UIImageView(frame: .zero)
    image.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    image.clipsToBounds = true
    image.isUserInteractionEnabled = true
    image.translatesAutoresizingMaskIntoConstraints = false
    image.contentMode = .scaleAspectFill
    return image
  }()
  
  private let pickButton: UIButton! = {
    let button = UIButton()
    button.titleLabel?.minimumScaleFactor = 0.5
    button.titleLabel?.lineBreakMode = .byTruncatingTail
    button.translatesAutoresizingMaskIntoConstraints = false
    button.addTarget(self, action: #selector(choseAction), for: .touchUpInside)
    return button
  }()
  
  private let tableView: UITableView = {
    let tableView = UITableView()
    tableView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    tableView.separatorStyle = .none
    tableView.translatesAutoresizingMaskIntoConstraints = false
    return tableView
  }()
  
  //MARK: - Inititalize
  init() {
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
    
  private func setup() {
    backgroundColor = .lightGray
    setTableView()
    setImageWithButton()
  }
  
  //MARK: - Setup subviews
  private func setTableView() {
    displayColletion = TableViewDisplayCollection()
    displayColletion.items = []
    tableView.register(displayColletion.cellClass,
                       forCellReuseIdentifier: String(describing: displayColletion.cellClass))
    tableView.delegate = self
    tableView.dataSource = displayColletion
    addSubview(tableView)
    NSLayoutConstraint.activate([
      tableView.topAnchor.constraint(equalTo: topAnchor, constant: Constants.offset),
      tableView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Constants.offset),
      tableView.rightAnchor.constraint(equalTo: rightAnchor, constant: -Constants.offset)
    ])
  }
  
  private func setImageWithButton() {
    addSubview(photoImage)
    NSLayoutConstraint.activate([
      photoImage.topAnchor.constraint(equalTo: topAnchor, constant: Constants.offset),
      photoImage.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Constants.offset),
      photoImage.widthAnchor.constraint(equalTo: photoImage.heightAnchor),
      photoImage.leftAnchor.constraint(equalTo: leftAnchor, constant: Constants.offset),
      photoImage.rightAnchor.constraint(equalTo: tableView.leftAnchor, constant: -Constants.offset)
    ])
    
    photoImage.addSubview(pickButton)
    NSLayoutConstraint.activate([
      pickButton.topAnchor.constraint(equalTo: photoImage.topAnchor),
      pickButton.bottomAnchor.constraint(equalTo: photoImage.bottomAnchor),
      pickButton.leadingAnchor.constraint(equalTo: photoImage.leadingAnchor),
      pickButton.trailingAnchor.constraint(equalTo: photoImage.trailingAnchor)
    ])
  }
  
  //MARK: - Actions
  @objc func choseAction() {
    delegate?.didTappedPickButton()
  }
}

extension FilterView: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRowWithThrottle(time: Constants.cellSelectDelay, at: indexPath, animated: true)
    delegate?.didSelectFilter(at: indexPath)
  }
  
}
