import Foundation

struct FilterEntity {
  let name: String
  let strategy: EditorStrategy
  
  func toPlainObject() -> FilterPlainObject {
    return FilterPlainObject(name: name)
  }
}

