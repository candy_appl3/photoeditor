import UIKit

class EditorViewController: UIViewController {
  
  //MARK: - Properties
  private var resultTableView = UITableView()
  private var resultDisplayCollection: TableViewDisplayCollection<ResultTableViewCellModel>!
  private var filterView = FilterView()
  
  var output: EditorViewOutput!
  
  //MARK: - Initialization
  init() {
    super.init(nibName: nil, bundle: nil)
    EditorModuleInitializer.setup(viewInput: self)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupFilterView()
    setupResultTableView()
    output.viewIsReady()
  }
  
  //MARK: - Setup subviews
  private func setupFilterView() {
    filterView.delegate = self.output
    filterView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(filterView)
    NSLayoutConstraint.activate([
      filterView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
      filterView.leftAnchor.constraint(equalTo: view.leftAnchor),
      filterView.rightAnchor.constraint(equalTo: view.rightAnchor),
      filterView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.3)
      ])
  }
  
  private func setupResultTableView() {
    resultDisplayCollection = TableViewDisplayCollection()
    resultTableView.delegate = self
    resultTableView
      .register(resultDisplayCollection.cellClass,
                forCellReuseIdentifier: String(describing: resultDisplayCollection.cellClass))
    resultTableView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    resultTableView.rowHeight = Constants.resultRowHeight
    resultTableView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(resultTableView)
    NSLayoutConstraint.activate([
      resultTableView.topAnchor.constraint(equalTo: filterView.bottomAnchor),
      resultTableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: Constants.offset),
      resultTableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -Constants.offset),
      resultTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -Constants.offset)
    ])
  }
  
  private func configureFilterView(with plainObject: FilterViewPlainObject) {
    filterView.pickTitle = plainObject.pickTitle
    filterView.filters = plainObject.filters
    filterView.image = plainObject.image
  }
  
  private func configureResultTableView(with results: [ResultPlainObject]) {
    resultDisplayCollection.items = results
    resultTableView.dataSource = resultDisplayCollection
    resultTableView.reloadData()
  }
}

//MARK: - Extensions
extension EditorViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRowWithThrottle(time: Constants.cellSelectDelay, at: indexPath, animated: true)
    output.didSelectResult(at: indexPath)
  }
}

extension EditorViewController: EditorViewInput {
  
  func showAlert(alert: AlertFactory) {
    let alert = AlertHelper(factory: alert).make { [weak self] type in
      self?.output.didSelectAlertAction(with: type)
    }
    present(alert, animated: true)
  }
  
  func insert(result: ResultPlainObject, at index: IndexPath) {
    resultDisplayCollection.items.insert(result, at: index.item)
    resultTableView.insertRows(at: [index], with: .fade)
  }
  
  func update(result: ResultPlainObject, at index: IndexPath) {
    resultDisplayCollection.items[index.item] = result
    resultTableView.reloadRows(at: [index], with: .fade)
  }
  
  func remove(result: ResultPlainObject, at index: IndexPath) {
    resultDisplayCollection.items.remove(at: index.item)
    resultTableView.deleteRows(at: [index], with: .fade)
  }
    
  func setupInitialState(filterViewPlainObject: FilterViewPlainObject, results: [ResultPlainObject]) {
    configureFilterView(with: filterViewPlainObject)
    configureResultTableView(with: results)
  }
  
  func set(pickTitle: String?) {
    filterView.pickTitle = pickTitle
  }
  
  func setImageToEdit(image: UIImage?) {
    filterView.image = image
  }
  
  func update(results: [ResultPlainObject], at index: [IndexPath]) {
    resultDisplayCollection.items = results
    resultTableView.insertRows(at: index, with: .fade)
  }
}
