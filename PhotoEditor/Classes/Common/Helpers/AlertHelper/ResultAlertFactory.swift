import UIKit

struct ResultAlertFactory: AlertFactory {
  func make(completion: @escaping (AlertType) -> ()) -> UIAlertController {
    
    let alertController = UIAlertController(title: "What to do with the image?", message: nil,
                                            preferredStyle: .actionSheet)
    
    let saveAction = UIAlertAction(title: "Save to album", style: .default) { _ in
      completion(.result(action: .save))
    }
    
    let editingAction = UIAlertAction(title: "Use for editing", style: .default) { _ in
      completion(.result(action: .use))
    }
    
    let removeAction = UIAlertAction(title: "Remove", style: .default) { _ in
      completion(.result(action: .remove))
    }
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
    
    alertController.addAction(saveAction)
    alertController.addAction(editingAction)
    alertController.addAction(removeAction)
    alertController.addAction(cancelAction)
    return alertController
  }
}
