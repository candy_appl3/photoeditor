import UIKit

protocol EditorStrategy {
  func edit(image: UIImage) -> UIImage
}

protocol Editor {
  var strategy: EditorStrategy { get }
  func edit(image: UIImage) -> UIImage
}

struct PhotoEditor: Editor {
  
   let strategy: EditorStrategy
  
  init(strategy: EditorStrategy) {
    self.strategy = strategy
  }
  
  func edit(image: UIImage) -> UIImage {
    return strategy.edit(image: image)
  }

}
