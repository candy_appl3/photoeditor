import UIKit.UITableViewCell

extension UITableView {
  func dequeueReusableCell(for indexPath: IndexPath, with model: CellViewAnyModel) -> UITableViewCell {
    
    let cell = dequeueReusableCell(withIdentifier: String(describing: type(of: model).cellClass()),
                                                          for: indexPath)
    
    model.updateAppearance(of: cell, in: self, at: indexPath)
    model.setupDefault(on: cell)
    
    return cell
  }
  
  func deselectRowWithThrottle(time: Double, at indexPath: IndexPath, animated: Bool) {
    isUserInteractionEnabled = false
    deselectRow(at: indexPath, animated: animated)
    DispatchQueue.main.asyncAfter(deadline: .now() + time) { [weak self] in
      self?.isUserInteractionEnabled = true
    }
  }
}

