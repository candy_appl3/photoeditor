import UIKit

struct SourceAlertFactory: AlertFactory {
  func make(completion: @escaping (AlertType) -> ()) -> UIAlertController {
    
    let alertController = UIAlertController(title: "Choose image source", message: nil,
                                            preferredStyle: .actionSheet)
    
    let cameraAction = UIAlertAction(title: "Camera", style: .default) { _ in
      completion(.imagePickerSource(source: .camera))
    }
    
    let photoAction = UIAlertAction(title: "Photo Library", style: .default) { _ in
      completion(.imagePickerSource(source: .photoLibrary))
    }
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
    
    alertController.addAction(cameraAction)
    alertController.addAction(photoAction)
    alertController.addAction(cancelAction)
    return alertController
  }
}
