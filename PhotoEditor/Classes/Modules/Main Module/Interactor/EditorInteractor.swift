import Foundation

class EditorInteractor: EditorInteractorInput {
  
  weak var output: EditorInteractorOutput!
  
  private var filters: [FilterEntity] = []
  private var results: [ResultPlainObject] = []
  private var imageToEdit: ImagePlainObject?
  private var selectedItemIndex: IndexPath?
  
  //MARK: - EditorInteractorInput
  func getFiltersAndResults() {
    createInititalContent()
    let pickTitle = imageToEdit?.image == nil ? "Choose image" : nil
    let filterViewPlainObject = FilterViewPlainObject(filters: filters.map { $0.toPlainObject() },
                                          pickTitle: pickTitle, image: imageToEdit?.image)
    output.didRecieve(filterViewPlainObject: filterViewPlainObject, results: results)
  }
  
  func set(imagePlainObject: ImagePlainObject) {
    imageToEdit = imagePlainObject
  }
  
  func editUsingFilter(at indexPath: IndexPath) {
    guard let imagePlainObject = imageToEdit else { output.imageToEditIsNotAvailable(); return }
    let filter = filters[indexPath.item]
    let editor = EditorModuleConfigurator.photoEditor(with: filter.strategy)
    DispatchQueue.global(qos: .userInitiated).async { [weak self] in
      let image = editor.edit(image: imagePlainObject.image)
      let result = ResultPlainObject(imagePlainObject:
                                     ImagePlainObject(image: image), filterName: filter.name)
      self?.results.insert(result, at: 0)
      DispatchQueue.main.async {
        self?.output.didEndEditing(result: result, at: IndexPath(item: 0, section: 0))
      }
    }
  }
  
  func setSelectItem(at index: IndexPath) {
    selectedItemIndex = index
  }
  
  func didChoose(resultAction: AlertType.ResultAction) {
    guard let index = selectedItemIndex else { return }
    switch resultAction {
    case .remove: removeResult(at: index)
    case .save: saveResult(at: index)
    case .use: setImageToEdit(at: index)
    }
  }
  
  //MARK: - Result actions
  private func saveResult(at index: IndexPath) {
    let saver = EditorModuleConfigurator.librarySaver
    saver.save(image: results[index.item].imagePlainObject.image) { [weak self] success in
      if !success { self?.output.failureWhileSaving() }
    }
  }
  
  private func setImageToEdit(at index: IndexPath) {
    imageToEdit = results[index.item].imagePlainObject
    guard let image = imageToEdit else { return }
    output.didRecieve(imagePlainObject: image)
  }
  
  private func removeResult(at index: IndexPath) {
    let removedResult = results.remove(at: index.item)
    output.didRemove(result: removedResult, at: index)
  }
  
  //MARK: Initialize
  private func createInititalContent() {
    self.filters = [
      FilterEntity(name: "Black and White", strategy: BlackAndWhiteStrategy()),
      FilterEntity(name: "Rotate", strategy: RotateStrategy()),
      FilterEntity(name: "Mirrored", strategy: HorizontalFlipStrategy()),
      FilterEntity(name: "Noir", strategy: NoirStrategy())
    ]
  }
}
