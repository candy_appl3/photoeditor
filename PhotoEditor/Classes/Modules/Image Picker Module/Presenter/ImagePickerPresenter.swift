import Foundation

class ImagePickerPresenter: ImagePickerModuleInput, ImagePickerViewOutput,
ImagePickerInteractorOutput {
  
  weak var view: ImagePickerViewInput!
  var interactor: ImagePickerInteractorInput!
  var router: ImagePickerRouterInput!
  
  //MARK: - View output
  func viewIsReady() {
     interactor.getSource()
  }
  
  func didFinishPickingMedia(with info: [String : Any]) {
    interactor.didRecieve(info: info)
  }
  
  func imagePickerDidCancel() {
    router.dismiss()
  }
  
  //MARK: - Interactor input
  func didRecieve(source: ImagePickerSource) {
    view.setupInitialState(source: source)
  }
  
  func didRecieve(imagePlainObject: ImagePlainObject) {
    router.dismissBySending(imagePlainObject: imagePlainObject)
  }
  
  func failWhileFetchImage() {
    router.dismiss()
  }
  
  //MARK: - Module intput
  func configure(with source: ImagePickerSource) {
    interactor.set(source: source)
  }
  
}
