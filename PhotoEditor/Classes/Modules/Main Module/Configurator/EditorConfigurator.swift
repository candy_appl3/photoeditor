import UIKit

class EditorModuleConfigurator {
  
  static var librarySaver: ImageSaver {
    return ImageLibrarySaver()
  }
  
  static func photoEditor(with strategy: EditorStrategy) -> Editor {
    return PhotoEditor(strategy: strategy)
  }
  
  func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
    
    if let viewController = viewInput as? EditorViewController {
      configure(viewController: viewController)
    }
  }
  
  private func configure(viewController: EditorViewController) {
    
    let router = EditorRouter()
    router.viewController = viewController
    
    let presenter = EditorPresenter()
    presenter.view = viewController
    presenter.router = router
    
    let interactor = EditorInteractor()
    interactor.output = presenter
    
    presenter.interactor = interactor
    viewController.output = presenter
  }
  
}
