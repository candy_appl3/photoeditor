import UIKit

class ImagePickerModuleInitializer {

  private init() {}
  
  static func setup(viewInput: ImagePickerViewController) {
    let configurator = ImagePickerModuleConfigurator()
    configurator.configureModuleForViewInput(viewInput: viewInput)
  }
  
}
