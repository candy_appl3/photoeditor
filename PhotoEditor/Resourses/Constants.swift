import UIKit

struct Constants {
  static let offset: CGFloat = 5
  static let resultRowHeight: CGFloat = 100
  static let cellSelectDelay: Double = 0.2
}

struct Keys {
  static var kEditedImage: String {
    return UIImagePickerControllerEditedImage
  }
}
