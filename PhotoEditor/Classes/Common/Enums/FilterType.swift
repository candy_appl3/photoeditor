import Foundation

enum FilterType {
  case blackAndWhite, mirrored, rotate90
}
