import UIKit
import CoreImage

struct NoirStrategy: EditorStrategy {
  
  let filterName = "CIPhotoEffectNoir"
  
  func edit(image: UIImage) -> UIImage {
    let resultImage = BaseFilter.edit(with: filterName, image: image)
    return resultImage ?? image
  }
}
