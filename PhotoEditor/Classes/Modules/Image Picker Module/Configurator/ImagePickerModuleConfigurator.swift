import UIKit

class ImagePickerModuleConfigurator {
  
  func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
    
    if let viewController = viewInput as? ImagePickerViewController {
      configure(viewController: viewController)
    }
  }
  
  private func configure(viewController: ImagePickerViewController) {
    
    let router = ImagePickerRouter()
    router.viewController = viewController
    
    let presenter = ImagePickerPresenter()
    presenter.view = viewController
    presenter.router = router
    
    let interactor = ImagePickerInteractor()
    interactor.output = presenter
    
    presenter.interactor = interactor
    viewController.output = presenter
  }
  
}
