import UIKit

struct FilterTableViewCellModel: CellViewModel {

  let cellObject: FilterPlainObject
  
  func setup(on cell: UITableViewCell) {
    cell.textLabel?.text = cellObject.name
    cell.textLabel?.textAlignment = .center
  }
}
