import UIKit

protocol EditorViewOutput: FilterViewDelegate {
  func viewIsReady()
  func didSelectResult(at index: IndexPath)
  func didSelectAlertAction(with type: AlertType)
}

