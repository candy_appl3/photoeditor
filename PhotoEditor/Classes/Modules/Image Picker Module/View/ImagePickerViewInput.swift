import Foundation

protocol ImagePickerViewInput: class {
  func setupInitialState(source: ImagePickerSource)
}
