import UIKit.UITableView

class TableViewDisplayCollection<ViewModel>: NSObject, UITableViewDataSource
where ViewModel: CellViewModel {
  
  let cellClass = ViewModel.CellClass.self
  var items: [ViewModel.CellObject]!

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let model = ViewModel.init(cellObject: items[indexPath.item])
    return tableView.dequeueReusableCell(for: indexPath, with: model)
  }
  
}
