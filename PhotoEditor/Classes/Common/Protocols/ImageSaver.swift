import UIKit

protocol ImageSaver {
  func save(image: UIImage, completion: @escaping (Bool) -> ())
}
