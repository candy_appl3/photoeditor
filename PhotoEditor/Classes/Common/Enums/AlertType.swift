import UIKit

typealias ImagePickerSource = UIImagePickerControllerSourceType

enum AlertType {
 
  enum ResultAction {
    case save, remove, use
  }
  
  case imagePickerSource(source: ImagePickerSource)
  case result(action: ResultAction)
  
}
