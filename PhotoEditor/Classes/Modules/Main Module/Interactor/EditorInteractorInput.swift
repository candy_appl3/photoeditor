import UIKit

protocol EditorInteractorInput {
  func getFiltersAndResults()
  func editUsingFilter(at indexPath: IndexPath)
  func set(imagePlainObject: ImagePlainObject)
  func setSelectItem(at index: IndexPath)
  func didChoose(resultAction: AlertType.ResultAction)
}
