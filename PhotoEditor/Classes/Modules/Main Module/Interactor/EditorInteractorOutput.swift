import Foundation

protocol EditorInteractorOutput: class {
  func didRecieve(filterViewPlainObject: FilterViewPlainObject, results: [ResultPlainObject])
  func didRecieve(imagePlainObject: ImagePlainObject)
  func didEndEditing(result: ResultPlainObject, at index: IndexPath)
  func didRemove(result: ResultPlainObject, at index: IndexPath)
  func didUpdate(result: ResultPlainObject, at index: IndexPath)
  func imageToEditIsNotAvailable()
  func failureWhileSaving()
}
