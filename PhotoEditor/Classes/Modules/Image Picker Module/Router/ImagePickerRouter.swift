import UIKit

class ImagePickerRouter: ImagePickerRouterInput {

  var viewController: UIViewController!
  
  func dismissBySending(imagePlainObject: ImagePlainObject) {
    guard let parentVC = viewController.parent as? EditorViewController,
      let parentModuleInput = parentVC.output as? EditorModuleInput else { return }
    parentModuleInput.didRecieve(imagePlainObject: imagePlainObject)
    viewController.dismiss(animated: true)
  }
  
  func dismiss() {
    viewController.dismiss(animated: true)
  }
  
}
