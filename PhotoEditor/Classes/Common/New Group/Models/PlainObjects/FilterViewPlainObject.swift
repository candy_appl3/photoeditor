import UIKit

struct FilterViewPlainObject {
  let filters: [FilterPlainObject]
  let pickTitle: String?
  let image: UIImage?
}
