import Foundation

protocol ImagePickerInteractorOutput: class {
  
  func didRecieve(source: ImagePickerSource)
  func didRecieve(imagePlainObject: ImagePlainObject)
  func failWhileFetchImage()
  
}
