import Foundation

protocol EditorRouterInput {

  func showImagePickerController(using source: ImagePickerSource)
  func show(alert: AlertFactory, handler: @escaping (AlertType) -> ())
  
}
