import Foundation

protocol ImagePickerInteractorInput {

  func didRecieve(info: [String: Any])
  func set(source: ImagePickerSource)
  func getSource()
  
}
