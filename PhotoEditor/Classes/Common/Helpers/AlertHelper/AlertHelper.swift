import UIKit

protocol AlertFactory {
  func make(completion: @escaping (AlertType) -> ()) -> UIAlertController
}

struct AlertHelper {
  
  let factory: AlertFactory
  
  func make(completion: @escaping (AlertType) -> ()) -> UIAlertController {
    return factory.make() { completion($0) }
  }
}


