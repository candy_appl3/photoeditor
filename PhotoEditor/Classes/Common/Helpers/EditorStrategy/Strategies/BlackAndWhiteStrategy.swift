import UIKit
import CoreImage

struct BlackAndWhiteStrategy: EditorStrategy {
  
  private let filterName = "CIPhotoEffectMono"
  
  func edit(image: UIImage) -> UIImage {
    let resultImage = BaseFilter.edit(with: filterName, image: image)
    return resultImage ?? image
  }
  
}
