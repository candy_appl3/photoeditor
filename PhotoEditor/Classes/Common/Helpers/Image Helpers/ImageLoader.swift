import UIKit

class ImageLoader {
  
  static func load(from url: URL, completion: (UIImage?) ->()) {
    do {
      let imageData = try Data(contentsOf: url)
      return completion(UIImage(data: imageData))
    } catch {
      print("Error loading image : \(error)")
    }
    completion(nil)
  }
  
}
