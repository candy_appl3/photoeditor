import UIKit

class ImagePickerViewController: UIViewController, ImagePickerViewInput {
  
  var output: ImagePickerViewOutput!
  private var imagePicker: UIImagePickerController!
  
  //MARK: - Inititalize
  init() {
    super.init(nibName: nil, bundle: nil)
    ImagePickerModuleInitializer.setup(viewInput: self)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    imagePicker = UIImagePickerController()
    imagePicker.delegate = self
    imagePicker.modalPresentationStyle = .popover
    output.viewIsReady()
  }
  
  //MARK: - View Input
  func setupInitialState(source: ImagePickerSource) {
    if UIImagePickerController.isSourceTypeAvailable(source) {
      imagePicker.allowsEditing = true
      imagePicker.sourceType = source
      self.present(imagePicker, animated: true, completion: nil)
    } else {
      output.imagePickerDidCancel()
    }
  }
}

extension ImagePickerViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    imagePicker.dismiss(animated: true) { [weak self] in
      self?.output.imagePickerDidCancel()
    }
    
  }
  
  func imagePickerController(_ picker: UIImagePickerController,
                             didFinishPickingMediaWithInfo info: [String : Any]) {
    
    
    
    output.didFinishPickingMedia(with: info)
  }
}
