import Foundation

protocol EditorModuleInput: class {
  func didRecieve(imagePlainObject: ImagePlainObject)
}
