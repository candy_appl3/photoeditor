import UIKit

struct ImageExtractor {
  
  static func extract(from data: Any?) -> UIImage? {
    return data as? UIImage
  }
  
}
