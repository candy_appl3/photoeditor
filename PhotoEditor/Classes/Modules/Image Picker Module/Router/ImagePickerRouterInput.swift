import Foundation

protocol ImagePickerRouterInput {
  func dismissBySending(imagePlainObject: ImagePlainObject)
  func dismiss()
}
