import UIKit

class EditorModuleInitializer {

  private init() {}
  
  static func setup(viewInput: EditorViewController) {
    let configurator = EditorModuleConfigurator()
    configurator.configureModuleForViewInput(viewInput: viewInput)
  }
  
}
