import UIKit

protocol EditorViewInput: class {
  func setupInitialState(filterViewPlainObject: FilterViewPlainObject, results: [ResultPlainObject])
  func setImageToEdit(image: UIImage?)
  func set(pickTitle: String?)
  func insert(result: ResultPlainObject, at index: IndexPath)
  func update(result: ResultPlainObject, at index: IndexPath)
  func remove(result: ResultPlainObject, at index: IndexPath)
  func showAlert(alert: AlertFactory)
}
