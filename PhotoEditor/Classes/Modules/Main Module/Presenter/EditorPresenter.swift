import Foundation

class EditorPresenter: EditorModuleInput, EditorViewOutput, EditorInteractorOutput {
  
  weak var view: EditorViewInput!
  var interactor: EditorInteractorInput!
  var router: EditorRouterInput!
  
  //MARK: - EditorViewOutput
  func viewIsReady() {
    interactor.getFiltersAndResults()    
  }
  
  func didSelectResult(at index: IndexPath) {
    interactor.setSelectItem(at: index)
    view.showAlert(alert: ResultAlertFactory())
  }
  
  func didTappedPickButton() {
    view.showAlert(alert: SourceAlertFactory())
  }
  
  func didSelectFilter(at indexPath: IndexPath) {
    interactor.editUsingFilter(at: indexPath)
  }
  
  func didSelectAlertAction(with type: AlertType) {
    switch type {
    case .imagePickerSource(let source): router.showImagePickerController(using: source)
    case .result(let action): interactor.didChoose(resultAction: action)
    }
  }
  
  //MARK: - EditorInteractorOutput
  
  func didEndEditing(result: ResultPlainObject, at index: IndexPath) {
    view.insert(result: result, at: index)
  }
  
  func didRemove(result: ResultPlainObject, at index: IndexPath) {
    view.remove(result: result, at: index)
  }
  
  func didUpdate(result: ResultPlainObject, at index: IndexPath) {
    view.update(result: result, at: index)
  }
  
  func didRecieve(filterViewPlainObject: FilterViewPlainObject, results: [ResultPlainObject]) {
    view.setupInitialState(filterViewPlainObject: filterViewPlainObject, results: results)
  }
  
  func imageToEditIsNotAvailable() {
    view.showAlert(alert: SourceAlertFactory())
  }
  
  func failureWhileSaving() {
    didTappedPickButton()
  }
  
  //MARK: Module Input
  func didRecieve(imagePlainObject: ImagePlainObject) {
    view.setImageToEdit(image: imagePlainObject.image)
    view.set(pickTitle: nil)
    interactor.set(imagePlainObject: imagePlainObject)
  }
}
