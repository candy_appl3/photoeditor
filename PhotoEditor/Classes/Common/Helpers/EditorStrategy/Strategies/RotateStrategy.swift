import UIKit
import CoreImage

struct RotateStrategy: EditorStrategy {
  
  func edit(image: UIImage) -> UIImage {
    let angle = CGFloat.pi / 2
    let rotateTransform = CGAffineTransform(rotationAngle: angle)
    var size = CGRect(origin: .zero, size: image.size).applying(rotateTransform).size
    size.width = round(size.width)
    size.height = round(size.height)
    
    UIGraphicsBeginImageContextWithOptions(size, true, image.scale)
    let context = UIGraphicsGetCurrentContext()
    context?.translateBy(x: size.width / 2, y: size.height / 2)
    context?.rotate(by: angle)
    
    image.draw(in: CGRect(x: -image.size.width / 2, y: -image.size.height / 2,
                          width: image.size.width, height: image.size.height))
    
    let resultImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return resultImage ?? image
  }
  
}
