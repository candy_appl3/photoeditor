import UIKit

struct ResultTableViewCellModel: CellViewModel {
  
  let cellObject: ResultPlainObject
  
  func setup(on cell: ResultTableViewCell) {
    cell.filterLabel.text = cellObject.filterName
    cell.filterLabel.sizeToFit()
    cell.resultImageView.image = cellObject.imagePlainObject.image
  }
}
