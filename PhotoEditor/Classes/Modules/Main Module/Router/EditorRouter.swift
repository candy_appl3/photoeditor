import UIKit

class EditorRouter: EditorRouterInput {

  var viewController: UIViewController!
  
  func showImagePickerController(using source: ImagePickerSource) {
    let imagePicker = ImagePickerViewController()
    guard let moduleInput = imagePicker.output as? ImagePickerModuleInput else { return }
    viewController.addChildViewController(imagePicker)
    moduleInput.configure(with: source)
    imagePicker.loadViewIfNeeded()
  }
  
  func show(alert: AlertFactory, handler: @escaping (AlertType) -> ()) {
    let alertVC = alert.make { type in
      handler(type)
    }
    viewController.present(alertVC, animated: true)
  }
  
}
