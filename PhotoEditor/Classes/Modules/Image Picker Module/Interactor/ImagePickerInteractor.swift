import Foundation

class ImagePickerInteractor: ImagePickerInteractorInput {
  
  weak var output: ImagePickerInteractorOutput!
  private var source: ImagePickerSource!
  
  //MARK: - Input
  func set(source: ImagePickerSource) {
    self.source = source
  }
  
  func getSource() {
    output.didRecieve(source: source)
  }
  
  func didRecieve(info: [String : Any]) {
    guard let image = ImageExtractor.extract(from: info[Keys.kEditedImage]) else {
      output.failWhileFetchImage()
      return
    }
    output.didRecieve(imagePlainObject: ImagePlainObject(image: image))
  }
  
}
