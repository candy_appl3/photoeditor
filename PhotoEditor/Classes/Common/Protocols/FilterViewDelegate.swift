import Foundation

protocol FilterViewDelegate: class {
  func didTappedPickButton()
  func didSelectFilter(at indexPath: IndexPath)
}

